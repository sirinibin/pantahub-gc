//
// Copyright 2018  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package routes

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pantacor/pantahub-gc/models"

	"gitlab.com/pantacor/pantahub-base/utils"
)

// MarkAllTrailGarbages : Mark trails as garbage that lost their parent device
func MarkAllTrailGarbages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if utils.GetEnv("DEBUG") == "true" {
		log.Println("Inside PUT /processgarbages/trail Handler")
	}
	response := models.MarkAllTrailGarbages()
	if len(response.Errors) > 0 {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(response)
}

// ProcessTrailGarbages : Find all trail documents with gc_processed=false then mark it associated steps as garbages
func ProcessTrailGarbages(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if utils.GetEnv("DEBUG") == "true" {
		log.Println("Inside PUT /processgarbages/trail Handler")
	}
	trail := &models.Trail{}
	response := trail.ProcessTrailGarbages()
	if len(response.Errors) > 0 {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(response)
}

// PopulateTrailsUsedObjects : Populate used_objects_field for all trails
func PopulateTrailsUsedObjects(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if utils.GetEnv("DEBUG") == "true" {
		log.Println("Inside PUT /populate/usedobjects/trails Handler")
	}
	trail := &models.Trail{}
	response := trail.PopulateAllTrailsUsedObjects()
	if len(response.Errors) > 0 {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}
	json.NewEncoder(w).Encode(response)
}
