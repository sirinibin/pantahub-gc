//
// Copyright 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package routes

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"

	"gitlab.com/pantacor/pantahub-gc/models"
)

// CronResponse : Response of PUT /cron
type CronResponse struct {
	Status                         int                                      `json:"status"`
	StatusMessage                  string                                   `json:"status_message"`
	DeleteDeviceGarbages           models.DeleteGarbagesResponse            `json:"delete_device_garbages"`
	MarkTrailGarbages              models.MarkTrailGarbagesResponse         `json:"mark_trail_garbages"`
	MarkUnclaimedDevicesAsGarbages models.MarkUnclaimedDevicesResponse      `json:"mark_unclaimed_devices_as_garbages"`
	MarkDeviceSummaryResponse      models.MarkDeviceSummaryResponse         `json:"mark_device_summary_garbages"`
	PopulateStepsUsedObjects       models.PopulateStepsUsedObjectsResponse  `json:"populate_steps_used_objects"`
	PopulateTrailsUsedObjects      models.PopulateTrailsUsedObjectsResponse `json:"populate_trails_used_objects"`
	ProcessDeviceGarbages          models.ProcessDeviceGarbagesResponse     `json:"process_device_garbages"`
	ProcessTrailGarbages           models.ProcessTrailGarbagesResponse      `json:"process_trail_garbages"`
	ProcessStepGarbages            models.ProcessStepGarbagesResponse       `json:"process_step_garbages"`
}

// Cron : handler function for GET /cron call
func Cron(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	log.Print("Inside Cron job Handler: PUT /cron")

	response := CronResponse{
		Status:        1,
		StatusMessage: "SUCCESS",
	}
	//Populate trails used objects
	trail := &models.Trail{}
	response.PopulateTrailsUsedObjects = trail.PopulateAllTrailsUsedObjects()
	if len(response.PopulateTrailsUsedObjects.Errors) > 0 {
		log.Print("Failed populate/usedobjects/trails : Populate trails used objects")
		logErrors(response.PopulateTrailsUsedObjects.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}
	// Populate steps used objects
	step := &models.Step{}
	response.PopulateStepsUsedObjects = step.PopulateAllStepsUsedObjects()
	if len(response.PopulateStepsUsedObjects.Errors) > 0 {
		log.Print("Failed PUT populate/usedobjects/steps : Populate steps used objects")
		logErrors(response.PopulateStepsUsedObjects.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}
	// Mark Unclaimed Devices as Garbage
	device := &models.Device{}
	response.MarkUnclaimedDevicesAsGarbages = device.MarkUnClaimedDevicesAsGarbage()
	if len(response.MarkUnclaimedDevicesAsGarbages.Errors) > 0 {
		log.Print("Failed PUT /markgarbage/devices/unclaimed : Mark Unclaimed Devices as Garbage")
		logErrors(response.MarkUnclaimedDevicesAsGarbages.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}

	// Mark Deleted Devices as Garbage in Device Summary
	device = &models.Device{}
	response.MarkDeviceSummaryResponse = device.MarkDeviceSummaryGarbages()
	if len(response.MarkDeviceSummaryResponse.Errors) > 0 {
		log.Print("Failed PUT /markgarbage/devicesummary : Mark Deleted Devices as Garbage in Device Summary")
		logErrors(response.MarkDeviceSummaryResponse.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}

	// Mark Trail as Garbage that lost their parent device
	response.MarkTrailGarbages = models.MarkAllTrailGarbages()
	if len(response.MarkTrailGarbages.Errors) > 0 {
		log.Print("Failed PUT /markgarbage/trails : Mark Trail as Garbage that lost their parent device")
		logErrors(response.MarkTrailGarbages.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}
	// Process Device Garbages
	device = &models.Device{}
	response.ProcessDeviceGarbages = device.ProcessDeviceGarbages()
	if len(response.ProcessDeviceGarbages.Errors) > 0 {
		log.Print("Failed PUT /processgarbages/devices : Process Device Garbages")
		logErrors(response.ProcessDeviceGarbages.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}
	// Process Trail Garbages
	trail = &models.Trail{}
	response.ProcessTrailGarbages = trail.ProcessTrailGarbages()
	if len(response.ProcessTrailGarbages.Errors) > 0 {
		log.Print("Failed PUT /processgarbages/trails : Process Trail Garbages")
		logErrors(response.ProcessTrailGarbages.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}
	// Process Step Garbages
	step = &models.Step{}
	response.ProcessStepGarbages = step.ProcessStepGarbages()
	if len(response.ProcessStepGarbages.Errors) > 0 {
		log.Print("Failed PUT /processgarbages/steps : Process Step Garbages")
		logErrors(response.ProcessStepGarbages.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}

	// DELETE device Garbages
	device = &models.Device{}
	response.DeleteDeviceGarbages = device.DeleteGarbages()
	if len(response.DeleteDeviceGarbages.Errors) > 0 {
		log.Print("Failed DELETE /devices : DELETE device Garbages")
		logErrors(response.DeleteDeviceGarbages.Errors)
		response.Status = 0
		response.StatusMessage = "FAIL"
	}

	if response.Status == 1 {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	log.Print("Response of PUT /cron")
	logResponse(response)
	json.NewEncoder(w).Encode(response)
}
func logErrors(errs url.Values) {
	log.Print("Errors:")
	log.Print(errs)
}
func logResponse(res CronResponse) {
	b, _ := json.MarshalIndent(res, "", "    ")
	log.Print(string(b))
}
