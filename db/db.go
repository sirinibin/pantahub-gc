//
// Copyright 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package db

import (
	"context"
	"log"
	"sync"
	"testing"
	"time"

	"gitlab.com/pantacor/pantahub-base/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var once sync.Once

var (
	clientInstance *mongo.Client
)

// Client : function to get Mongo Client
func Client() *mongo.Client {

	once.Do(func() { // <-- atomic, does not allow repeating
		clientInstance = Connect()
	})

	return clientInstance
}

// Connect : To connect to the mongoDb
func Connect() *mongo.Client {
	//mongo-go-driver settings
	mongoClient, err := GetMongoClient()
	if err != nil {
		panic(err)
	}
	return mongoClient
}

// ConnectTest : To connect to the mongoDb for test
func ConnectTest(t *testing.T) *mongo.Client {
	//mongo-go-driver settings
	mongoClient, err := GetMongoClient()
	if err != nil {
		t.Error(err)
	}
	return mongoClient
}

// GetPantahubBaseDB : Get Pantahub Base DB
func GetPantahubBaseDB() string {
	return utils.GetEnv("MONGO_DB")
}

// GetDevSummaryDb : Get Dev Summary Db
func GetDevSummaryDb() string {
	return "pantabase_devicesummary"
}

// GetMongoClient : To Get Mongo Client Object
func GetMongoClient() (*mongo.Client, error) {
	mongoHost := utils.GetEnv("MONGO_HOST")
	mongoPort := utils.GetEnv("MONGO_PORT")
	mongoUser := utils.GetEnv("MONGO_USER")
	mongoPass := utils.GetEnv("MONGO_PASS")
	mongoRs := utils.GetEnv("MONGO_RS")

	//Setting Client Options
	clientOptions := options.Client()

	credentials := options.Credential{
		AuthMechanism: "SCRAM-SHA-1",
		AuthSource:    GetPantahubBaseDB(),
		Username:      mongoUser,
		Password:      mongoPass,
		PasswordSet:   true,
	}
	mongoConnect := "mongodb://" + mongoHost + ":" + mongoPort
	clientOptions.SetAuth(credentials)
	clientOptions.SetReplicaSet(mongoRs)
	clientOptions.ApplyURI(mongoConnect)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	MongoClient, err := mongo.Connect(ctx, clientOptions)
	log.Println("Will connect to mongo PROD db with: " + mongoConnect)

	return MongoClient, err
}
