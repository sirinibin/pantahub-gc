//
// Copyright 2018-2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package models

import (
	"context"
	"log"
	"net/url"
	"strconv"
	"strings"
	time "time"

	"gitlab.com/pantacor/pantahub-base/objects"
	"gitlab.com/pantacor/pantahub-base/utils"
	"gitlab.com/pantacor/pantahub-gc/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

// Trail : Trail model
type Trail struct {
	ID     primitive.ObjectID `json:"id" bson:"_id"`
	Owner  string             `json:"owner"`
	Device string             `json:"device"`
	//  Admins   []string `json:"admins"`   // XXX: maybe this is best way to do delegating device access....
	LastInSync       time.Time              `json:"last-insync" bson:"last-insync"`
	LastTouched      time.Time              `json:"last-touched" bson:"last-touched"`
	FactoryState     map[string]interface{} `json:"factory-state" bson:"factory-state"`
	Garbage          bool                   `json:"garbage" bson:"garbage"`
	GarbageRemovalAt time.Time              `bson:"garbage_removal_at" json:"garbage_removal_at"`
	GcProcessed      bool                   `json:"gc_processed" bson:"gc_processed"`
	UsedObjects      []string               `bson:"used_objects" json:"used_objects"`
}

// ProcessTrailGarbagesResponse : response of ProcessTrailGarbages() function
type ProcessTrailGarbagesResponse struct {
	Status                 int        `json:"status"`
	StatusMessage          string     `json:"status_message"`
	TrailsProcessed        int        `json:"trails_processed"`
	StepsMarkedAsGarbage   int        `json:"steps_marked_as_garbage"`
	ObjectsMarkedAsGarbage int        `json:"objects_marked_as_garbage"`
	TrailsWithErrors       int        `json:"trails_with_errors"`
	ObjectsWithErrors      int        `json:"objects_with_errors"`
	ObjectsIgnored         int        `json:"objects_ignored"`
	StepsWithErrors        int        `json:"steps_with_errors"`
	Errors                 url.Values `json:"errors"`
	Warnings               url.Values `json:"warnings"`
}

// PopulateUsedObjectsResponse : Populate UsedObject Response
type PopulateUsedObjectsResponse struct {
	Status                   int        `json:"status"`
	StatusMessage            string     `json:"status_message"`
	ObjectList               []string   `json:"object_list"`
	InvalidObjectCount       int        `json:"invalid_object_count"`
	InvalidState             bool       `json:"invalid_state"`
	ObjectsUnMarkedAsGarbage []string   `json:"objects_unmarked_as_garbage"`
	Errors                   url.Values `json:"errors"`
}

// PopulateTrailsUsedObjectsResponse : Populate Trails Used Objects Response
type PopulateTrailsUsedObjectsResponse struct {
	Status                   int        `json:"status"`
	StatusMessage            string     `json:"status_message"`
	TrailsPopulated          int        `json:"trails_populated"`
	TrailsWithErrors         int        `json:"trails_with_errors"`
	ObjectsUnMarkedAsGarbage []string   `json:"objects_unmarked_as_garbage"`
	Errors                   url.Values `json:"errors"`
}

// MarkTrailGarbagesResponse : response of MarkAllTrailGarbages() function
type MarkTrailGarbagesResponse struct {
	Status        int        `json:"status"`
	StatusMessage string     `json:"status_message"`
	TrailsMarked  int        `json:"trails_marked"`
	Errors        url.Values `json:"errors"`
}

// DeleteTrailsResponse : response of deleteTrailGarbage() function
type DeleteTrailsResponse struct {
	Status        int        `json:"status"`
	StatusMessage string     `json:"status_message"`
	TrailsRemoved int        `json:"trails_removed"`
	Errors        url.Values `json:"errors"`
}

// MarkAllTrailGarbages : Mark all trail garbages
func MarkAllTrailGarbages() (response MarkTrailGarbagesResponse) {
	response.Errors = url.Values{}
	response.TrailsMarked = 0

	trailsWithErrors := []string{}

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage": bson.M{"$ne": true}}, findOptions)
	if err != nil {
		response.Errors.Add("find_trails", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		trail := Trail{}
		err = cur.Decode(&trail)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		if contains(trailsWithErrors, trail.ID.Hex()) {
			continue
		}
		deviceResult, deviceErrors := IsDeviceValid(trail.ID)
		if len(deviceErrors) > 0 {
			response.Errors = MergeErrors(response.Errors, deviceErrors)
			trailsWithErrors = append(trailsWithErrors, trail.ID.Hex())
		}
		if !deviceResult {
			trailResult, trailErrors := markTrailAsGarbage(trail.ID)
			if len(trailErrors) > 0 {
				response.Errors = MergeErrors(response.Errors, trailErrors)
			}
			if trailResult {
				response.TrailsMarked++
			}
		}
	}

	if err := cur.Err(); err != nil {
		response.Errors.Add("cursor_error", err.Error())
	}
	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}

// markTrailAsGarbage : Mark Trail A sGarbage
func markTrailAsGarbage(deviceID primitive.ObjectID) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	GarbageRemovalAt, parseErrors := GetGarbageRemovalTime()
	if len(parseErrors) > 0 {
		errs = MergeErrors(errs, parseErrors)
		return false, errs
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	updateResult, err := collection.UpdateOne(
		ctx,
		bson.M{"_id": deviceID},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
			"gc_processed":       false,
		}},
	)
	if err != nil {
		errs.Add("mark_trail_as_garbage", err.Error()+"[Trail ID:"+deviceID.Hex()+"]")
		return false, errs
	}
	if updateResult.MatchedCount == 0 {
		errs.Add("mark_trail_as_garbage", "Invalid trail [Trail ID:"+deviceID.Hex()+"]")
		return false, errs
	}

	trail := Trail{}
	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = collection.FindOne(ctx,
		bson.M{"_id": deviceID}).
		Decode(&trail)
	if err != nil {
		errs.Add("trail", err.Error())
	}
	// keep used_objects of trails as upto date
	trail.populateTrailUsedObjects()
	// keep used_objects of step(rev=0) as upto date
	step := Step{}
	step.ID = trail.ID.Hex() + "-0"
	step.Owner = trail.Owner
	step.State = trail.FactoryState
	step.populateStepsUsedObjects()

	return true, errs
}

// ProcessTrailGarbages : Process Trail Garbages
func (trail *Trail) ProcessTrailGarbages() (response ProcessTrailGarbagesResponse) {
	response.Errors = url.Values{}
	response.Warnings = url.Values{}
	response.TrailsProcessed = 0
	response.StepsMarkedAsGarbage = 0
	response.ObjectsMarkedAsGarbage = 0
	response.TrailsWithErrors = 0
	response.ObjectsWithErrors = 0
	response.ObjectsIgnored = 0
	response.StepsWithErrors = 0

	objectsIgnored := []string{}
	objectsMarkedAsGarbageList := []string{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	// Fetch all trail documents with (garbage:true AND (gc_processed:false if exist OR gc_processed not exist ))
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage":      true,
		"gc_processed": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		response.Errors.Add("find_trails", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for i := 0; cur != nil && cur.Next(ctx); i++ {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		trail := Trail{}
		err = cur.Decode(&trail)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		if utils.GetEnv("DEBUG") == "true" {
			log.Print("Processing Trail:" + strconv.Itoa(i) + "[ID:" + trail.ID.Hex() + "]")
		}

		populateResponse := trail.populateTrailUsedObjects()
		if len(populateResponse.Errors) > 0 {
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Found Errors in Trail while populating used_objects:" + strconv.Itoa(i) + "[ID:" + trail.ID.Hex() + "]")
				log.Print(populateResponse.Errors)
			}
			response.Errors = MergeErrors(response.Errors, populateResponse.Errors)
			response.TrailsWithErrors++
			if populateResponse.InvalidObjectCount > 0 {
				response.ObjectsWithErrors += populateResponse.InvalidObjectCount
				response.StepsWithErrors++ //for steps with rev=0
			}
		} else {
			//1.Mark Steps As Garbage
			_, stepsMarked, stepErrors := markStepsAsGarbage(trail.ID)
			response.StepsMarkedAsGarbage += stepsMarked
			if len(stepErrors) > 0 {
				response.Errors = MergeErrors(response.Errors, stepErrors)
				response.StepsWithErrors++
			}
			//2.Mark Objects As Garbage
			_,
				objectsMarkedList,
				objectsWithErrors,
				newObjectsIgnoredList,
				_,
				objectErrors := markObjectsAsGarbage(trail.UsedObjects)
			response.ObjectsWithErrors += objectsWithErrors

			if len(objectErrors) > 0 {
				response.Errors = MergeErrors(response.Errors, objectErrors)
			}
			/* Populating objects Marked  list,Note: Purpose of
			these 2 below for loops are to avoid counts of duplicate marked/ignored
			objects count when there is same object is used in multiple trails/steps */
			for _, v := range objectsMarkedList {
				if !contains(objectsMarkedAsGarbageList, v) {
					objectsMarkedAsGarbageList = append(objectsMarkedAsGarbageList, v)
				}
			}
			//populating igonored object list
			for _, v := range newObjectsIgnoredList {
				if !contains(objectsIgnored, v) {
					objectsIgnored = append(objectsIgnored, v)
				}
			}

		}
		//Marking trail as gc_processed=true
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": trail.ID},
			bson.M{"$set": bson.M{"gc_processed": true}},
		)
		if err != nil {
			response.Errors.Add("marking_trail_as_gc_processed", err.Error()+"[ID:"+trail.ID.Hex()+"]")
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Error Marking Trail:" + strconv.Itoa(i) + " As Processed [ID:" + trail.ID.Hex() + "]")
			}
		}
		if updateResult.ModifiedCount == 1 {
			response.TrailsProcessed++
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Marked Trail:" + strconv.Itoa(i) + " As Processed [ID:" + trail.ID.Hex() + "]")
			}
		}
	} //end for loop

	//populate Marked Objects Count
	response.ObjectsMarkedAsGarbage += len(objectsMarkedAsGarbageList)
	//Populating warning messages for ignored objects
	if len(objectsIgnored) > 0 {
		for _, v := range objectsIgnored {
			response.Warnings.Add("objects_ignored", "Ignorinng trail Object storage-id(_id):"+v+" due to more than 0 usage")
		}
	}
	//populate Ignored Objects Count
	response.ObjectsIgnored = len(objectsIgnored)
	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}

// populateTrailUsedObjects : Populate used_objects field in trail collection
func (trail *Trail) populateTrailUsedObjects() (response PopulateUsedObjectsResponse) {
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	response = parseStateObjects(trail.Owner, trail.FactoryState, "Trail ID:"+trail.ID.Hex())

	if utils.GetEnv("DEBUG") == "true" {
		log.Print("Populated " + strconv.Itoa(len(trail.UsedObjects)) + " Used Objects for trail id:" + trail.ID.Hex())
		log.Print("\n")
	}
	trail.UsedObjects = response.ObjectList
	if response.InvalidObjectCount == 0 && !response.InvalidState {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": trail.ID},
			bson.M{"$set": bson.M{
				"used_objects": response.ObjectList,
			}},
		)
		if err != nil {
			response.Errors.Add("update_trail_used_object", err.Error()+"[ID:"+trail.ID.Hex()+"]")
			response.Status = 0
			response.StatusMessage = "FAIL"
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Not Populated used_objects List for trail id:" + trail.ID.Hex())
				log.Print(response)
				log.Print("\n")
			}
			return response
		}
		if updateResult.ModifiedCount == 1 {
			response.Status = 1
			response.StatusMessage = "SUCCESS"
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Populated " + strconv.Itoa(len(trail.UsedObjects)) + " Used Objects for trail id:" + trail.ID.Hex())
				log.Print("\n")
			}
		} else {
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
		return response
	}
	response.Status = 0
	response.StatusMessage = "FAIL"
	if utils.GetEnv("DEBUG") == "true" {
		log.Print("Not Populated used_objects List for trail id:" + trail.ID.Hex())
		log.Print(response)
		log.Print("\n")
	}
	return response
}

// parseStateObjects: Parse State Objects and populate ObjectList
func parseStateObjects(
	owner string,
	state map[string]interface{},
	model string,
) (
	response PopulateUsedObjectsResponse,
) {
	response.Errors = url.Values{}
	response.ObjectList = []string{}
	response.InvalidObjectCount = 0
	response.ObjectsUnMarkedAsGarbage = []string{}
	response.Status = 0
	response.InvalidState = false

	objMap := map[string]bool{}

	spec, ok := state["#spec"]
	if !ok {
		response.Errors.Add("state_object", "Invalid state:#spec is missing ["+model+"]")
		response.InvalidState = true
		return response
	}

	specValue, ok := spec.(string)
	if !ok {
		response.Errors.Add("state_object", "Invalid state:Value of #spec should be string ["+model+"]")
		response.InvalidState = true
		return response
	}

	if specValue != "pantavisor-multi-platform@1" &&
		specValue != "pantavisor-service-system@1" {
		response.Errors.Add("state_object", "Invalid state:Value of #spec should not be "+specValue+" ["+model+"]")
		response.InvalidState = true
		return response
	}

	for key, v := range state {
		if strings.HasSuffix(key, ".json") ||
			strings.HasSuffix(key, "Ｎjson") ||
			key == "#spec" {
			continue
		}
		sha, found := v.(string)
		if !found {
			response.Errors.Add("state_object", "Object is not a string[sha:"+sha+","+model+"]")
			response.InvalidObjectCount++
			continue
		}
		shaBytes, err := utils.DecodeSha256HexString(sha)
		if err != nil {
			response.Errors.Add("state_object", "Object sha that could not be decoded from hex:"+err.Error()+" [sha:"+sha+","+model+"]")
			response.InvalidObjectCount++
			continue
		}
		// lets use proper storage shas to reflect that fact that each
		// owner has its own copy of the object instance on DB side
		storageSha := objects.MakeStorageId(owner, shaBytes)
		result, _ := IsObjectValid(storageSha)
		if !result {
			response.Errors.Add("state_object", "Object sha is not found in the db[storage-id(_id):"+storageSha+","+model+"]")
			response.InvalidObjectCount++
			continue
		}
		if _, ok := objMap[storageSha]; !ok {
			result, objectErrors := IsObjectGarbage(storageSha)
			if len(objectErrors) > 0 {
				response.Errors = MergeErrors(response.Errors, objectErrors)
			}
			if result {
				unMarkResult, objectErrors := UnMarkObjectAsGarbage(storageSha)
				if len(objectErrors) > 0 {
					response.Errors = MergeErrors(response.Errors, objectErrors)
				}
				if unMarkResult {
					//Populate UnMarked Objects As Garbage List
					response.ObjectsUnMarkedAsGarbage = append(response.ObjectsUnMarkedAsGarbage, storageSha)
				}
			}
			objMap[storageSha] = true
			response.ObjectList = append(response.ObjectList, storageSha)
		}
	}
	if response.InvalidObjectCount > 0 {
		return response
	}
	response.Status = 1
	return response
}

// deleteTrailGarbage : Delete Trail Garbage
func deleteTrailGarbage() (response DeleteTrailsResponse) {
	response.Errors = url.Values{}
	response.TrailsRemoved = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{
			"garbage":            true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		response.Errors.Add("remove_all_trails", err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.TrailsRemoved = int(info.DeletedCount)
			return response
		}
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.TrailsRemoved = int(info.DeletedCount)
	return response
}

// PopulateAllTrailsUsedObjects : Populate used_objects_field for all trails
func (trail *Trail) PopulateAllTrailsUsedObjects() (response PopulateTrailsUsedObjectsResponse) {
	response.Errors = url.Values{}
	response.ObjectsUnMarkedAsGarbage = []string{}
	response.TrailsPopulated = 0
	response.TrailsWithErrors = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	// Fetch all trail documents without used_objects field
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"used_objects": bson.M{"$exists": false},
	}, findOptions)
	if err != nil {
		response.Errors.Add("find_trails", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		trail := Trail{}
		err = cur.Decode(&trail)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		populateResponse := trail.populateTrailUsedObjects()
		if len(populateResponse.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, populateResponse.Errors)
		}
		if populateResponse.InvalidObjectCount == 0 && !populateResponse.InvalidState {
			response.TrailsPopulated++
		} else {
			response.TrailsWithErrors++
		}
		//Merge  Objects UnMarked As Garbage
		response.ObjectsUnMarkedAsGarbage = append(response.ObjectsUnMarkedAsGarbage, populateResponse.ObjectsUnMarkedAsGarbage...)
	}

	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}

// contains : to check if array contains an item or not
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
