//
// Copyright 2018-2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package models

import (
	"context"
	"log"
	"net/url"
	time "time"

	"gitlab.com/pantacor/pantahub-base/utils"
	"gitlab.com/pantacor/pantahub-gc/db"
	"gopkg.in/mgo.v2/bson"
)

// Object : Object model
type Object struct {
	ID               string `json:"id" bson:"id"`
	StorageID        string `json:"storage-id" bson:"_id"`
	Owner            string `json:"owner"`
	ObjectName       string `json:"objectname"`
	Sha              string `json:"sha256sum"`
	Size             string `json:"size"`
	SizeInt          int64  `json:"sizeint"`
	MimeType         string `json:"mime-type"`
	initialized      bool
	Garbage          bool      `json:"garbage" bson:"garbage"`
	GarbageRemovalAt time.Time `bson:"garbage_removal_at" json:"garbage_removal_at"`
}

// DeleteObjectsResponse : response of deleteAllObjectsGarbage() function
type DeleteObjectsResponse struct {
	Status         int        `json:"status"`
	StatusMessage  string     `json:"status_message"`
	ObjectsRemoved int        `json:"objects_removed"`
	Errors         url.Values `json:"errors"`
}

// markObjectsAsGarbage : Mark Objects as Garbage
func markObjectsAsGarbage(objects []string) (
	result bool,
	objectsMarkedAsGarbageList []string,
	objectsWithErrors int,
	objectsIgnored []string,
	objectsIgnoredCount int,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_objects")
	GarbageRemovalAt, parseErrors := GetGarbageRemovalTime()
	if len(parseErrors) > 0 {
		errs = MergeErrors(errs, parseErrors)
		return false, nil, 0, nil, 0, errs
	}
	objectsMarkedAsGarbage := 0
	objectsWithErrors = 0
	objectsIgnoredCount = 0
	objectsMarkedAsGarbageList = []string{}

	for _, object := range objects {
		result, _ := IsObjectValid(object)
		if !result {
			objectsWithErrors++
		}
		totalUsage, _, _,
			usageErrors := getObjectUsageInfo(object)
		if len(usageErrors) > 0 {
			errs = MergeErrors(errs, usageErrors)
			log.Print("Skipping object:" + object + " Due to Errors while finding the usage count")
			objectsIgnored = append(objectsIgnored, object)
			objectsIgnoredCount++
			continue
		}
		log.Print("TOTAL OBJECT USAGE:")
		log.Print(totalUsage)
		if totalUsage > 0 {
			log.Print("Ignoring object:" + object)
			objectsIgnored = append(objectsIgnored, object)
			objectsIgnoredCount++
		} else {
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("\nObject:" + object)
			}
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			updateResult, err := collection.UpdateOne(
				ctx,
				bson.M{"_id": object},
				bson.M{"$set": bson.M{
					"garbage":            true,
					"garbage_removal_at": GarbageRemovalAt,
				}},
			)
			if err != nil {
				errs.Add("mark_object_as_garbage", err.Error()+"[Object ID:"+object+"]")
			}
			if updateResult.ModifiedCount == 1 &&
				!contains(objectsMarkedAsGarbageList, object) {
				objectsMarkedAsGarbage++
				objectsMarkedAsGarbageList = append(objectsMarkedAsGarbageList, object)
			}
		}
	}
	if len(errs) > 0 {
		return false,
			objectsMarkedAsGarbageList,
			objectsWithErrors,
			objectsIgnored,
			objectsIgnoredCount,
			errs
	}
	return true,
		objectsMarkedAsGarbageList,
		objectsWithErrors,
		objectsIgnored,
		objectsIgnoredCount,
		errs
}

// deleteAllObjectsGarbage : Delete Objects  Garbage
func deleteAllObjectsGarbage() (response DeleteObjectsResponse) {
	response.Errors = url.Values{}
	response.ObjectsRemoved = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_objects")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{
			"garbage":            true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		response.Errors.Add("remove_all_objects", err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.ObjectsRemoved = int(info.DeletedCount)
			return response
		}
		return response
	}
	if utils.GetEnv("DEBUG") == "true" {
		log.Print("Objects removed:")
		log.Print(info.DeletedCount)
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.ObjectsRemoved = int(info.DeletedCount)
	return response
}

// getObjectUsageInfo : Get Object Usage Information
func getObjectUsageInfo(objectString string) (
	totalUsage int,
	usageInTrails int,
	usageInSteps int,
	errs url.Values,
) {
	errs = url.Values{}
	trailsCollection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")
	stepsCollection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_trails")

	//Find Usage count in trails
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	count, err := trailsCollection.CountDocuments(ctx, bson.M{
		"used_objects": objectString,
		"garbage":      bson.M{"$ne": true},
	})
	if err != nil {
		errs.Add("find_object_used_count_in_trails", err.Error())
	}
	usageInTrails = int(count)

	//Find Usage count in steps
	ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	count, err = stepsCollection.CountDocuments(ctx, bson.M{
		"used_objects": objectString,
		"garbage":      bson.M{"$ne": true},
	})
	if err != nil {
		errs.Add("find_object_used_count_in_steps", err.Error())
	}
	usageInSteps = int(count)
	totalUsage = (usageInTrails + usageInSteps)
	return totalUsage, usageInTrails, usageInSteps, errs
}

// IsObjectValid : to check if an object is valid or not
func IsObjectValid(ObjectID string) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_objects")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	count, err := collection.CountDocuments(ctx, bson.M{
		"_id": ObjectID,
	})
	if err != nil {
		errs.Add("object", err.Error())
	}
	return (count == 1), errs
}

// IsObjectGarbage : to check if an object is garbage or not
func IsObjectGarbage(ObjectID string) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_objects")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	count, err := collection.CountDocuments(ctx, bson.M{
		"_id":     ObjectID,
		"garbage": true,
	})
	if err != nil {
		errs.Add("object", err.Error())
	}
	return (count == 1), errs
}

// UnMarkObjectAsGarbage : to unmark object as garbage
func UnMarkObjectAsGarbage(ObjectID string) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_objects")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.UpdateOne(
		ctx,
		bson.M{"_id": ObjectID},
		bson.M{"$set": bson.M{
			"garbage": false,
		}},
	)
	if err != nil {
		errs.Add("unmark_object_as_garbage", err.Error()+"[ID:"+ObjectID+"]")
		return false, errs
	}
	return true, errs
}
