//
// Copyright 2018-2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package models

import (
	"context"
	"log"
	"net/url"
	"strconv"
	time "time"

	"gitlab.com/pantacor/pantahub-base/utils"
	"gitlab.com/pantacor/pantahub-gc/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

// step wanted can be added by the device owner or delegate.
// steps that were not reported can be deleted still. other steps
// cannot be deleted until the device gets deleted as well.
// Step: Step model
type Step struct {
	ID               string                 `json:"id" bson:"_id"` // XXX: make type
	Owner            string                 `json:"owner"`
	Device           string                 `json:"device"`
	Committer        string                 `json:"committer"`
	TrailID          primitive.ObjectID     `json:"trail-id" bson:"trail-id"` //parent id
	Rev              int                    `json:"rev"`
	CommitMsg        string                 `json:"commit-msg" bson:"commit-msg"`
	State            map[string]interface{} `json:"state"` // json blurb
	StateSha         string                 `json:"state-sha" bson:"statesha"`
	StepProgress     StepProgress           `json:"progress" bson:"progress"`
	StepTime         time.Time              `json:"step-time" bson:"step-time"`
	ProgressTime     time.Time              `json:"progress-time" bson:"progress-time"`
	Garbage          bool                   `json:"garbage" bson:"garbage"`
	GarbageRemovalAt time.Time              `bson:"garbage_removal_at" json:"garbage_removal_at"`
	GcProcessed      bool                   `json:"gc_processed" bson:"gc_processed"`
	UsedObjects      []string               `bson:"used_objects" json:"used_objects"`
}

// StepProgress : StepProgress model
type StepProgress struct {
	Progress  int    `json:"progress"`                    // progress number. steps or 1-100
	StatusMsg string `json:"status-msg" bson:"statusmsg"` // message of progress status
	Status    string `json:"status"`                      // status code
	Log       string `json:"log"`                         // log if available
}

// ProcessStepGarbagesResponse : Response of ProcessStepGarbages() function
type ProcessStepGarbagesResponse struct {
	Status                 int        `json:"status"`
	StatusMessage          string     `json:"status_message"`
	StepsProcessed         int        `json:"steps_processed"`
	ObjectsMarkedAsGarbage int        `json:"objects_marked_as_garbage"`
	ObjectsWithErrors      int        `json:"objects_with_errors"`
	StepsWithErrors        int        `json:"steps_with_errors"`
	ObjectsIgnored         int        `json:"objects_ignored"`
	Errors                 url.Values `json:"errors"`
	Warnings               url.Values `json:"warnings"`
}

// PopulateStepsUsedObjectsResponse : Populate Steps Used Objects Response
type PopulateStepsUsedObjectsResponse struct {
	Status                   int        `json:"status"`
	StatusMessage            string     `json:"status_message"`
	StepsPopulated           int        `json:"steps_populated"`
	StepsWithErrors          int        `json:"steps_with_errors"`
	ObjectsUnMarkedAsGarbage []string   `json:"objects_unmarked_as_garbage"`
	Errors                   url.Values `json:"errors"`
}

// DeleteStepsResponse : response of deleteStepsGarbage() function
type DeleteStepsResponse struct {
	Status        int        `json:"status"`
	StatusMessage string     `json:"status_message"`
	StepsRemoved  int        `json:"steps_removed"`
	Errors        url.Values `json:"errors"`
}

// markStepsAsGarbage : Mark Steps Aa Garbage
func markStepsAsGarbage(deviceID primitive.ObjectID) (
	result bool,
	stepsMarked int,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_steps")
	GarbageRemovalAt, parseErrors := GetGarbageRemovalTime()
	if len(parseErrors) > 0 {
		errs = MergeErrors(errs, parseErrors)
		return false, 0, errs
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.UpdateMany(ctx,
		bson.M{
			"trail-id": deviceID,
		},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
			"gc_processed":       false,
		}},
	)
	if utils.GetEnv("DEBUG") == "true" {
		log.Printf("%+v\n", info)
	}
	if err != nil {
		errs.Add("mark_all_trail_steps_as_garbage", err.Error()+"[trail-id:"+deviceID.Hex()+"]")
		if info != nil {
			return false, int(info.ModifiedCount), errs
		}
		return false, 0, errs
	}
	return true, int(info.ModifiedCount), errs
}

// ProcessStepGarbages : Process Step Garbages
func (step *Step) ProcessStepGarbages() (response ProcessStepGarbagesResponse) {
	response.Errors = url.Values{}
	response.Warnings = url.Values{}
	response.StepsProcessed = 0
	response.ObjectsMarkedAsGarbage = 0
	response.StepsWithErrors = 0
	response.ObjectsWithErrors = 0
	response.ObjectsIgnored = 0

	objectsIgnored := []string{}
	objectsMarkedAsGarbageList := []string{}

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_steps")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	// Fetch all steps documents with (garbage:true AND (gc_processed:false if exist OR gc_processed not exist ))
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next()*/
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage":      true,
		"gc_processed": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		response.Errors.Add("find_steps", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for i := 0; cur != nil && cur.Next(ctx); i++ {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		step := Step{}
		err = cur.Decode(&step)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		if utils.GetEnv("DEBUG") == "true" {
			log.Print("Processing Step:" + strconv.Itoa(i) + "[ID:" + step.ID + "]")
		}
		populateResponse := step.populateStepsUsedObjects()
		if len(populateResponse.Errors) > 0 {
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Found Errors in Step while populating used_objects:" + strconv.Itoa(i) + "[ID:" + step.ID + "]")
				log.Print(populateResponse.Errors)
			}
			response.Errors = MergeErrors(response.Errors, populateResponse.Errors)
			response.StepsWithErrors++
		}
		_,
			objectsMarkedList,
			objectsWithErrors,
			newObjectsIgnoredList,
			_,
			objectErrors := markObjectsAsGarbage(step.UsedObjects)

		/*populating objects Marked  list,Note: Purpose of
		these 2 below for loops are to avoid counts of duplicate marked/ignored
		objects count when there is same object is used in multiple trails/steps */
		for _, v := range objectsMarkedList {
			if !contains(objectsMarkedAsGarbageList, v) {
				objectsMarkedAsGarbageList = append(objectsMarkedAsGarbageList, v)
			}
		}
		//populating igonored object list
		for _, v := range newObjectsIgnoredList {
			if !contains(objectsIgnored, v) {
				objectsIgnored = append(objectsIgnored, v)
			}
		}
		response.ObjectsWithErrors += objectsWithErrors
		if len(objectErrors) > 0 {
			response.Errors = MergeErrors(response.Errors, objectErrors)
		}
		//Marking step as gc_processed=true
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": step.ID},
			bson.M{"$set": bson.M{
				"gc_processed": true,
			}},
		)
		if err != nil {
			response.Errors.Add("mark_step_as_gc_processed", err.Error()+"[Step ID:"+step.ID+"]")
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Error Marking Step:" + strconv.Itoa(i) + " As Processed [ID:" + step.ID + "]")
			}
		}
		if updateResult.ModifiedCount == 1 {
			response.StepsProcessed++
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Marked Step:" + strconv.Itoa(i) + " As Processed [ID:" + step.ID + "]")
			}
		}
	} //end for loop
	//populate Marked Objects Count
	response.ObjectsMarkedAsGarbage += len(objectsMarkedAsGarbageList)
	//Populating warning messages for ignored objects
	if len(objectsIgnored) > 0 {
		for _, v := range objectsIgnored {
			response.Warnings.Add("objects_ignored", "Ignorinng step Object storage-id(_id):"+v+" due to more than 0 usage")
		}
	}
	response.ObjectsIgnored = len(objectsIgnored)

	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}

// populateUsedObjects : Populate used_objects field in trail
func (step *Step) populateStepsUsedObjects() (response PopulateUsedObjectsResponse) {
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_steps")
	response = parseStateObjects(step.Owner, step.State, "Step ID:"+step.ID)
	step.UsedObjects = response.ObjectList
	if response.InvalidObjectCount == 0 && !response.InvalidState {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": step.ID},
			bson.M{"$set": bson.M{
				"used_objects": response.ObjectList,
			}},
		)
		if err != nil {
			response.Errors.Add("update_steps_used_objects", err.Error()+"[Step ID:"+step.ID+"]")
		}
		if utils.GetEnv("DEBUG") == "true" {
			log.Print("Populated " + strconv.Itoa(len(step.UsedObjects)) + " Used Objects for step id:" + step.ID)
			log.Print("\n")
		}
		if updateResult.ModifiedCount == 1 {
			response.Status = 1
			response.StatusMessage = "SUCCESS"
		} else {
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
		return response
	}
	response.Status = 0
	response.StatusMessage = "FAIL"
	if utils.GetEnv("DEBUG") == "true" {
		log.Print("Not Populated used_objects List for step id:" + step.ID)
		log.Print(response)
		log.Print("\n")
	}
	return response
}

// deleteStepsGarbage : Delete Steps  Garbage
func deleteStepsGarbage() (response DeleteStepsResponse) {
	response.Errors = url.Values{}
	response.StepsRemoved = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_steps")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{
			"garbage":            true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		response.Errors.Add("delete_all_steps", err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.StepsRemoved = int(info.DeletedCount)
			return response
		}
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.StepsRemoved = int(info.DeletedCount)
	return response
}

// PopulateAllStepsUsedObjects : Populate used_objects_field for all steps
func (step *Step) PopulateAllStepsUsedObjects() (response PopulateStepsUsedObjectsResponse) {
	response.Errors = url.Values{}
	response.ObjectsUnMarkedAsGarbage = []string{}
	response.StepsPopulated = 0
	response.StepsWithErrors = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_steps")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	// Fetch all steps documents without used_objects field
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	cur, err := collection.Find(ctx, bson.M{
		"used_objects": bson.M{"$exists": false}}, findOptions)
	if err != nil {
		response.Errors.Add("find_steps", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		step := Step{}
		err = cur.Decode(&step)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		populateResponse := step.populateStepsUsedObjects()
		if len(populateResponse.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, populateResponse.Errors)
			response.StepsWithErrors++
		}
		if populateResponse.InvalidObjectCount == 0 &&
			!populateResponse.InvalidState {
			response.StepsPopulated++
		}
		//Merge  Objects UnMarked As Garbage
		response.ObjectsUnMarkedAsGarbage = append(response.ObjectsUnMarkedAsGarbage, populateResponse.ObjectsUnMarkedAsGarbage...)
	}
	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}
