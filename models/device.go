//
// Copyright 2018-2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package models

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"strconv"
	time "time"

	duration "github.com/ChannelMeter/iso8601duration"
	"github.com/gorilla/mux"
	"gitlab.com/pantacor/pantahub-base/utils"
	"gitlab.com/pantacor/pantahub-gc/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

// Device : Device model
type Device struct {
	ID               primitive.ObjectID     `json:"id" bson:"_id"`
	Prn              string                 `json:"prn"`
	Nick             string                 `json:"nick"`
	Owner            string                 `json:"owner"`
	OwnerNick        string                 `json:"owner-nick" bson:"-"`
	Secret           string                 `json:"secret,omitempty"`
	TimeCreated      time.Time              `json:"time-created" bson:"timecreated"`
	TimeModified     time.Time              `json:"time-modified" bson:"timemodified"`
	Challenge        string                 `json:"challenge,omitempty"`
	IsPublic         bool                   `json:"public"`
	UserMeta         map[string]interface{} `json:"user-meta" bson:"user-meta"`
	DeviceMeta       map[string]interface{} `json:"device-meta" bson:"device-meta"`
	Garbage          bool                   `json:"garbage" bson:"garbage"`
	GarbageRemovalAt time.Time              `bson:"garbage_removal_at" json:"garbage_removal_at"`
	GcProcessed      bool                   `json:"gc_processed" bson:"gc_processed"`
}

// ProcessDeviceGarbagesResponse : response of ProcessDeviceGarbages function
type ProcessDeviceGarbagesResponse struct {
	Status                int        `json:"status"`
	StatusMessage         string     `json:"status_message"`
	DeviceProcessed       int        `json:"device_processed"`
	TrailsMarkedAsGarbage int        `json:"trails_marked_as_garbage"`
	TrailsWithErrors      int        `json:"trails_with_errors"`
	Errors                url.Values `json:"errors"`
}

// MarkUnclaimedDevicesResponse : response of MarkUnClaimedDevicesAsGarbage() function
type MarkUnclaimedDevicesResponse struct {
	Status        int        `json:"status"`
	StatusMessage string     `json:"status_message"`
	DevicesMarked int        `json:"devices_marked"`
	Errors        url.Values `json:"errors"`
}

// MarkDeviceSummaryResponse : response of MarkDeviceSummaryGarbages() function
type MarkDeviceSummaryResponse struct {
	Status        int        `json:"status"`
	StatusMessage string     `json:"status_message"`
	DevicesMarked int        `json:"devices_marked"`
	Errors        url.Values `json:"errors"`
}

// DeleteGarbagesResponse : Response of  DeleteGarbages() function
type DeleteGarbagesResponse struct {
	Status        int                         `json:"status"`
	StatusMessage string                      `json:"status_message"`
	Devices       DeleteDevicesResponse       `json:"devices"`
	DeviceSummary DeleteDeviceSummaryResponse `json:"device_summary"`
	Trails        DeleteTrailsResponse        `json:"trails"`
	Steps         DeleteStepsResponse         `json:"steps"`
	Objects       DeleteObjectsResponse       `json:"objects"`
	Errors        url.Values                  `json:"errors"`
}

// DeleteDevicesResponse : response of deleteDeviceGarbage() function
type DeleteDevicesResponse struct {
	Status         int        `json:"status"`
	StatusMessage  string     `json:"status_message"`
	DevicesRemoved int        `json:"devices_removed"`
	Errors         url.Values `json:"errors"`
}

// DeleteDeviceSummaryResponse : response of deleteDeviceSummaryGarbage() function
type DeleteDeviceSummaryResponse struct {
	Status         int        `json:"status"`
	StatusMessage  string     `json:"status_message"`
	DevicesRemoved int        `json:"devices_removed"`
	Errors         url.Values `json:"errors"`
}

// MarkDeviceAsGarbage : Mark a device as garbage
func (device *Device) MarkDeviceAsGarbage() (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	GarbageRemovalAt, parseErrors := GetGarbageRemovalTime()
	if len(parseErrors) > 0 {
		errs = MergeErrors(errs, parseErrors)
		return false, errs
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.UpdateOne(
		ctx,
		bson.M{"_id": device.ID},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
		}},
	)
	if err != nil {
		errs.Add("mark_device_as_garbage", err.Error()+"[ID:"+device.ID.Hex()+"]")
		return false, errs
	}
	device.Garbage = true
	device.GarbageRemovalAt = GarbageRemovalAt
	return true, errs
}

// MarkUnClaimedDevicesAsGarbage :  Mark all unclaimed devices as garbage after a while(eg: after 5 days)
func (device *Device) MarkUnClaimedDevicesAsGarbage() (response MarkUnclaimedDevicesResponse) {
	response.Errors = url.Values{}
	response.DevicesMarked = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	GarbageRemovalAt, parseErrors := GetGarbageRemovalTime()
	if len(parseErrors) > 0 {
		response.Errors = MergeErrors(response.Errors, parseErrors)
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	TimeLeftForGarbaging := utils.GetEnv("PANTAHUB_GC_UNCLAIMED_EXPIRY")
	parsedDuration, err := duration.FromString(TimeLeftForGarbaging)
	if err != nil {
		response.Errors.Add("parsing_iso8601", err.Error()+"[ID:"+device.ID.Hex()+"]")
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	TimeBeforeDuration := time.Now().Local().Add(-parsedDuration.ToDuration())
	if utils.GetEnv("DEBUG") == "true" {
		log.Print("TimeBeforeDuration:")
		log.Print(TimeBeforeDuration)
		log.Print("GarbageRemovalAt:")
		log.Print(GarbageRemovalAt)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Hour)
	defer cancel()
	info, err := collection.UpdateMany(ctx,
		bson.M{
			"challenge":   bson.M{"$ne": ""},
			"timecreated": bson.M{"$lt": TimeBeforeDuration},
			"garbage":     bson.M{"$ne": true},
		},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt,
		}},
	)

	if err != nil {
		response.Errors.Add("mark_unclaimed_device_as_garbage", err.Error()+"[ID:"+device.ID.Hex()+"]")
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.DevicesMarked = int(info.ModifiedCount)
			return response
		}
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.DevicesMarked = int(info.ModifiedCount)
	return response
}

// ProcessDeviceGarbages : Process Device Garbages
func (device *Device) ProcessDeviceGarbages() (response ProcessDeviceGarbagesResponse) {
	response.Errors = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	//Fetch all device documents with (garbage:true AND (gc_processed:false if exist OR gc_processed not exist ))
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next()
	*/
	cur, err := collection.Find(ctx, bson.M{
		"garbage":      true,
		"gc_processed": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		response.Errors.Add("find_devices", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	response.DeviceProcessed = 0
	response.TrailsMarkedAsGarbage = 0
	response.TrailsWithErrors = 0
	for i := 0; cur != nil && cur.Next(ctx); i++ {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		device := Device{}
		err = cur.Decode(&device)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		if utils.GetEnv("DEBUG") == "true" {
			log.Print("Processing Device:" + strconv.Itoa(i) + "[ID:" + device.ID.Hex() + "]")
		}

		trailResult, trailErrs := markTrailAsGarbage(device.ID)
		if len(trailErrs) > 0 {
			response.Errors = MergeErrors(response.Errors, trailErrs)
			response.TrailsWithErrors++
		}
		if trailResult {
			response.TrailsMarkedAsGarbage++
		}

		//Marking device as gc_processed=true
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		updateResult, err := collection.UpdateOne(
			ctx,
			bson.M{"_id": device.ID},
			bson.M{"$set": bson.M{"gc_processed": true}},
		)
		if err != nil {
			response.Errors.Add("mark_device_as_gc_processed", err.Error()+"[ID:"+device.ID.Hex()+"]")
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Error Marking Device:" + strconv.Itoa(i) + " As Processed [ID:" + device.ID.Hex() + "]")
			}
		}
		if updateResult.MatchedCount > 0 {
			response.DeviceProcessed++
			if utils.GetEnv("DEBUG") == "true" {
				log.Print("Marked Device:" + strconv.Itoa(i) + " As Processed [ID:" + device.ID.Hex() + "]")
			}
		}
	} //end for loop

	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}

// DeleteGarbages : Delete Garbages of a device
func (device *Device) DeleteGarbages() (response DeleteGarbagesResponse) {
	if utils.GetEnv("PANTAHUB_GC_REMOVE_GARBAGE") == "true" {
		//Enabled
		// 1.Delete Devices
		response.Devices = deleteDeviceGarbage()
		if len(response.Trails.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, response.Devices.Errors)
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
		// 2.Delete Device Summary
		response.DeviceSummary = deleteDeviceSummaryGarbage()
		if len(response.Trails.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, response.Devices.Errors)
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
		// 3.Delete Trails
		response.Trails = deleteTrailGarbage()
		if len(response.Trails.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, response.Trails.Errors)
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
		// 4.Delete Steps
		response.Steps = deleteStepsGarbage()
		if len(response.Steps.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, response.Steps.Errors)
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
		// 5.Delete Objects
		response.Objects = deleteAllObjectsGarbage()
		if len(response.Objects.Errors) > 0 {
			response.Errors = MergeErrors(response.Errors, response.Objects.Errors)
			response.Status = 0
			response.StatusMessage = "FAIL"
		}
	} else {
		//Disabled
		response.Status = 1
		response.Errors = url.Values{}
		response.StatusMessage = "DISABLED"

		response.Devices = DeleteDevicesResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Errors:        url.Values{},
		}
		response.DeviceSummary = DeleteDeviceSummaryResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Errors:        url.Values{},
		}
		response.Trails = DeleteTrailsResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Errors:        url.Values{},
		}
		response.Steps = DeleteStepsResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Errors:        url.Values{},
		}

		response.Objects = DeleteObjectsResponse{
			Status:        1,
			StatusMessage: "DISABLED",
			Errors:        url.Values{},
		}
	}
	return response
}

// Validate : Validate PUT devices/{id} request
func (device *Device) Validate(r *http.Request) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	params := mux.Vars(r)
	deviceID, err := primitive.ObjectIDFromHex(params["id"])
	if err != nil {
		errs.Add("id", "Invalid Document ID"+"[ID:"+params["id"]+"]")
		return false, errs
	}
	device.ID = deviceID
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = collection.FindOne(ctx,
		bson.M{"_id": device.ID}).
		Decode(&device)
	if err != nil {
		errs.Add("device_find_error", err.Error()+"[ID:"+device.ID.Hex()+"]")
		return false, errs
	}
	return true, nil
}

// MergeErrors : Merge 2 arrays of Errors
func MergeErrors(errors url.Values, newErrors url.Values) url.Values {
	if len(newErrors) > 0 {
		for key, v := range newErrors {
			errors.Add(key, v[0])
		}
	}
	return errors
}

// GetGarbageRemovalTime : Get Garbage Removal Time
func GetGarbageRemovalTime() (time.Time, url.Values) {
	errors := url.Values{}
	RemoveGarbageAfter := utils.GetEnv("PANTAHUB_GC_GARBAGE_EXPIRY")
	parsedDuration, err := duration.FromString(RemoveGarbageAfter)
	if err != nil {
		errors.Add("parsing_iso8601", err.Error())
	}
	GarbageRemovalAt := time.Now().Local().Add(parsedDuration.ToDuration())
	return GarbageRemovalAt, errors
}

// IsDeviceValid : to check if a DeviceID is valid or not
func IsDeviceValid(DeviceID primitive.ObjectID) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	count, err := collection.CountDocuments(ctx, bson.M{
		"_id": DeviceID,
	})
	if err != nil {
		errs.Add("device", err.Error())
	}
	return (count == 1), errs
}

// MarkDeviceSummaryGarbages : Mark Deleted Devices As Garbage In DeviceSummary
func (device *Device) MarkDeviceSummaryGarbages() (response MarkDeviceSummaryResponse) {
	response.Errors = url.Values{}
	response.DevicesMarked = 0

	devicesWithErrors := []string{}

	collection := db.Client().Database(db.GetDevSummaryDb()).Collection("device_summary_short_new_v2")
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetNoCursorTimeout(true)
	/* Note: Actual Record fetching will not happen here
	as it is using mongodb cursor and record fetching will
	start with we call cur.Next() */
	// Create mongo cursor
	cur, err := collection.Find(ctx, bson.M{
		"garbage": bson.M{"$ne": true},
	}, findOptions)
	if err != nil {
		response.Errors.Add("find_device_summary", err.Error())
	}
	if cur != nil {
		defer cur.Close(ctx)
	}
	for cur != nil && cur.Next(ctx) {
		err := cur.Err()
		if err != nil {
			response.Errors.Add("cursor_error", err.Error())
			break
		}
		result := map[string]interface{}{}
		err = cur.Decode(&result)
		if err != nil {
			response.Errors.Add("cursor_decode_error", err.Error())
			continue
		}
		deviceID := result["deviceid"].(string)
		if contains(devicesWithErrors, deviceID) {
			continue
		}
		deviceObjectID, err := primitive.ObjectIDFromHex(result["deviceid"].(string))
		if err != nil {
			response.Errors.Add("invalid_hex", err.Error())
			continue
		}
		deviceResult, deviceErrors := IsDeviceValid(deviceObjectID)
		if len(deviceErrors) > 0 {
			response.Errors = MergeErrors(response.Errors, deviceErrors)
			devicesWithErrors = append(devicesWithErrors, deviceID)
		}
		if !deviceResult {
			deviceResult, deviceErrors := markDeviceSummaryAsGarbage(deviceID)
			if len(deviceErrors) > 0 {
				response.Errors = MergeErrors(response.Errors, deviceErrors)
			}
			if deviceResult {
				response.DevicesMarked++
			}
		}
	}

	if err := cur.Err(); err != nil {
		response.Errors.Add("cursor_error", err.Error())
	}
	if len(response.Errors) > 0 {
		response.Status = 0
		response.StatusMessage = "FAIL"
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	return response
}

// markDeviceSummaryAsGarbage : Mark Device Summary As Garbage
func markDeviceSummaryAsGarbage(deviceID string) (
	result bool,
	errs url.Values,
) {
	errs = url.Values{}
	GarbageRemovalAt, parseErrors := GetGarbageRemovalTime()
	if len(parseErrors) > 0 {
		errs = MergeErrors(errs, parseErrors)
		return false, errs
	}
	collection := db.Client().Database(db.GetDevSummaryDb()).Collection("device_summary_short_new_v2")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := collection.UpdateOne(
		ctx,
		bson.M{"deviceid": deviceID},
		bson.M{"$set": bson.M{
			"garbage":            true,
			"garbage_removal_at": GarbageRemovalAt}},
	)
	if err != nil {
		errs.Add("mark_device_summary_as_garbage", err.Error()+"[Device ID:"+deviceID+"]")
		return false, errs
	}
	return true, errs
}

// deleteDeviceGarbage : Delete Device Garbages
func deleteDeviceGarbage() (response DeleteDevicesResponse) {
	response.Errors = url.Values{}
	response.DevicesRemoved = 0

	collection := db.Client().Database(db.GetPantahubBaseDB()).Collection("pantahub_devices")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{
			"garbage":            true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		response.Errors.Add("remove_device_garbages", err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.DevicesRemoved = int(info.DeletedCount)
			return response
		}
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.DevicesRemoved = int(info.DeletedCount)
	return response
}

// deleteDeviceSummaryGarbage : Delete Device Summary Garbages
func deleteDeviceSummaryGarbage() (response DeleteDeviceSummaryResponse) {
	response.Errors = url.Values{}
	response.DevicesRemoved = 0

	collection := db.Client().Database(db.GetDevSummaryDb()).Collection("evice_summary_short_new_v2")
	now := time.Now().Local()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	info, err := collection.DeleteMany(ctx,
		bson.M{"garbage": true,
			"garbage_removal_at": bson.M{"$lt": now},
		})
	if err != nil {
		response.Errors.Add("remove_device_summary_garbages", err.Error())
		response.Status = 0
		response.StatusMessage = "FAIL"
		if info != nil {
			response.DevicesRemoved = int(info.DeletedCount)
			return response
		}
		return response
	}
	response.Status = 1
	response.StatusMessage = "SUCCESS"
	response.DevicesRemoved = int(info.DeletedCount)
	return response
}
